import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestScreaming {

	 //Req--1 where Green phase is implemented 
	 @Test
	  void testOnePersonIsAmazing() {
	  Screaming s = new Screaming();
	  String result = s.scream("peter");
	  assertEquals("Peter is Amazing", result);
	  }
	
	 //Req2- where Red phase is implemented
     @Test
		void testNobodyIsListening() {
		 Screaming s = new Screaming();
		 String result = s.scream("");
		 assertEquals("You are Amazing", result);
		}
		
     //Req3- Red phase implemented 
     @Test
   		void testPeterIsShouting() {
   		 Screaming s = new Screaming();
   		 String result = s.scream("PETER");
   		 assertEquals("PETER IS AMAZING", result);
   		}
     
     //Req4- Green phase implemented 
     @Test
   		void testTwoPeopleAreShouting() {
   		 Screaming s = new Screaming();
   		 String result = s.scream("Peter" , "albert");
   		 assertEquals("Peter and albert are amazing", result);
   		}
     
     //Req5- Red phase implemented 
     @Test
   		void testMoreThanTwoPeopleAreShouting() {
   		 Screaming s = new Screaming();
   		 String result = s.scream("Peter, albert, jk");
   		 assertEquals("Peter, albert, and jk are amazing", result);
   		}
     
     //Req--6 Red phase implemented
     @Test
		void testMoreThanTwoPeople() {
		 Screaming s = new Screaming();
		 String result = s.scream("Peter, albert, JK");
		 assertEquals("Peter and albert are amazing , JK ALSO", result);
		}
	
   	
     
  
}
