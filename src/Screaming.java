/*
 		Requirements for the Tribal Screaming App
		1. One person is amazing
		2. Nobody is listening
		3. Peter is shouting
		4. Two (2) people are amazing
		5. More than 2 people are amazing
		6. Shouting a lot of people
*/
public class Screaming {
	private final String delimiter = "," ;
	
	public String scream(String name) {
		
		String[] Myname = name.split(delimiter);
		
		if(name == "")
		{
		    return "You are Amazing";	
		}
		else if(name == "PETER")
		{
			return "PETER IS AMAZING";
		}
		else if(name == "peter")
		{
		return "Peter is Amazing";
	}
		return getstring(Myname) + " are amazing";
	}

	//Refactor --req--4
	public String scream(String name1 , String name2) {
		String TwoPeople = "";
		TwoPeople = name1 + " and " + name2 + " are amazing";
		return TwoPeople;
	}
	
	//Req -5 : Refactor : String array to get more than 2 names
	 private String getstring(String[] Myname)
		{
			String finalOutput = "";
			for(int current = 0 ; current < Myname.length ; current++ )
			{
				if(current < Myname.length -1)
				{
					finalOutput = finalOutput + String.valueOf(Myname[current])  + ",";
			    }
				
				else
				{
					finalOutput = finalOutput + " and" +   String.valueOf(Myname[current])  ;
				}
			
			}
			return finalOutput;
		}
	
}
